package Activitat1;

import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JOptionPane;

public class Activitat1App {

	public static void main(String[] args) {
		//Primero pedimos cuantas notas quiere introducir i cuantos alumnos
		String textoNumeroAlumnos = JOptionPane.showInputDialog(null, "Cuantos alumnos hay en el curso?");
		int numeroAlumnos = Integer.parseInt(textoNumeroAlumnos);
		//Creo el hashtable
		Hashtable<String,Double> notasAlumnos = new Hashtable<String,Double>();
		//Relleno el hashtable
		notasAlumnos = hacerLaTabla(numeroAlumnos);
		//Muestro el hashtable
		mostrarHashtable(notasAlumnos);
		
	}
	//Este m�todo sirve para rellenar el hashtable, le pregunta que id tiene el alumno, i que nota tiene
	//despues se guarda la nota media
	public static Hashtable hacerLaTabla(int numeroAlumnos) {
		Hashtable<String,Double> notasAlumnos = new Hashtable<String,Double>();
		double total = 0;
		for (int i = 0; i < numeroAlumnos; i++) {
			total = 0;
			String idAlumno = JOptionPane.showInputDialog(null, "Que id tiene el " + (i + 1) + " alumno?");
			for (int x = 0; x < 4; x++) {
				String textoNotaAlumno = JOptionPane.showInputDialog(null, "Pon la nota " + (x + 1));
				int notaAlumno = Integer.parseInt(textoNotaAlumno);
				total = total + notaAlumno;
			}
			total = total / 4;
			
			notasAlumnos.put(idAlumno, total);
		}
		return notasAlumnos;
	}
	//Solo para mostrar un hashtable
	public static void mostrarHashtable(Hashtable<String,Double> table) {
		Enumeration<Double> elements = table.elements();
		Enumeration<String> llaves = table.keys();
		
		while (llaves.hasMoreElements()) {
			System.out.println("El alumno con la id " + llaves.nextElement() + " tiene una media de: " + elements.nextElement());
			
		}
	}

}
