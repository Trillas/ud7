package Activitat2;

import java.util.ArrayList;

import javax.swing.JOptionPane;


public class Activitat2App {

	public static void main(String[] args) {
		//Primero pedimos cuantos productos ha comprado
		double totalAPagar = 0;
		String textoNumeroProductos = JOptionPane.showInputDialog(null, "Cuantos productos has comprado en total?");
		int numeroProductos = Integer.parseInt(textoNumeroProductos);
		//Aqui creo el arrayList i hago un for para rellenar cada array dentro del array, tambien guardo el precio con iva 
		//para hacer el precio total
		ArrayList<String[]> producto = new ArrayList<String[]>();
		for (int i = 0; i < numeroProductos; i++) {
			producto.add(introducirProducto());
			totalAPagar = totalAPagar + Double.parseDouble(producto.get(i)[3]);
		}
		//Digo cuanto tiene que pagar i le muestro la lista con todos los productos i el total a pagar
		String textoPagado = JOptionPane.showInputDialog(null, "Tienes que pagar " + totalAPagar);
		double pagado = Integer.parseInt(textoPagado);
		mostrarLista(producto, numeroProductos);
		System.out.println("Has pagado " + pagado + " tu cambio es de " + (pagado - totalAPagar));
	}
	//En este metodo pido un producto el iva i que precio, tambien hago el total con iva y lo paso
	public static String[] introducirProducto() {
		String[] listaProducto = new String[4];
		String producto = JOptionPane.showInputDialog(null, "Que producto has comprado");
		listaProducto[0] = producto;
		String iva = JOptionPane.showInputDialog(null, "Que iva tiene el producto?(21 o 4)");
		listaProducto[1] = iva;
		String precioProducto = JOptionPane.showInputDialog(null, "Que precio tiene el producto?");
		listaProducto[2] = precioProducto;
		double precioTotal = ((Double.parseDouble(precioProducto) / 100) * Double.parseDouble(iva)) + Double.parseDouble(precioProducto);
		listaProducto[3] = String.valueOf(precioTotal);
		
		return listaProducto;
	}
	//Para mostrar el arrayList
	public static void mostrarLista(ArrayList<String[]> lista, int num) {
		
		for (int i = 0; i < num; i++) {
			for (int j = 0; j < num; j++) {
				System.out.println("Producto: " + lista.get(i)[0]);
				System.out.println("IVA: " + lista.get(i)[1]);
				System.out.println("Precio: " + lista.get(i)[2]);
				System.out.println("Precio con IVA: " + lista.get(i)[3]);
			}
			System.out.println(" ");
		}	
	}
}
