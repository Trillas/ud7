package Activitat4;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JOptionPane;


public class Activitat4 {

	public static void main(String[] args) {
		//Creo las variables que necesitare
		boolean salir = true;
		String txtQueHace;
		//Creo el Hashtable y lo lleno de productos
		Hashtable<String,Double> articulos = new Hashtable<String,Double>(){
			{
			put("CocaCola", 2.00);
			put("Pepsi", 1.4);
			put("Pollo", 4.3);
			put("Pan", 0.5);
			put("Ajo", 1.3);
			put("Pi�a", 2.5);
			put("Tomate", 3.1);
			put("Yogurt", 1.7);
			put("Helado", 3.5);
			put("Pescado", 4.9);
			}
		};
		do {
			//Hago la pregunta del menu, i compruebo que sea valido
			//He a�adido una opci�n para lanzar todo el ejercicio 2
			do {
				txtQueHace = JOptionPane.showInputDialog(null, "Que quieres hacer?(1: a�adir, 2: comprobar un producto, 3: mirar todos los productos, 4: Comprar Productos, 5: salir)");
			} while (!txtQueHace.contentEquals("1") && !txtQueHace.contentEquals("2") && !txtQueHace.contentEquals("3") && !txtQueHace.contentEquals("4") && !txtQueHace.contentEquals("5"));
			int queHace = Integer.parseInt(txtQueHace);
			switch (queHace) {
			//Hago un switch con el men� y las diferentes opciones
			case 1:
				//Pregunto nombre y precio del producto y los a�ado
				String nombreProducto = JOptionPane.showInputDialog(null, "Que nombre tiene el producto?");
				String txtPrecioProducto = JOptionPane.showInputDialog(null, "Que precio tiene el producto?");
				double precioProducto= Double.parseDouble(txtPrecioProducto);
				articulos.put(nombreProducto, precioProducto);
				
				break;
			case 2:
				//Miro el producto
				String productoMirar = JOptionPane.showInputDialog(null, "Que producto quieres comprobar?");
				JOptionPane.showMessageDialog(null, "El articulo " + productoMirar + " tiene un precio de: " + articulos.get(productoMirar));
				break;
			case 3:
				//Muestro todos los productos
				mostrarHashtable(articulos);
				break;
			case 4:
				//en esta opcion lanza un m�todo donde he puesto todo el ejercicio 2
				compraProductos(articulos);
				break;
			case 5:
				//Esto es que quiere salir, asi que le ense�o un mensage
				JOptionPane.showMessageDialog(null, "Adios");
				salir = false;
				break;
			}
		} while (salir);
		
	}
	public static void compraProductos(Hashtable<String,Double> articulos) {
		//Primero pedimos cuantos productos ha comprado
		double totalAPagar = 0;
		String textoNumeroProductos = JOptionPane.showInputDialog(null, "Cuantos productos has comprado en total?");
		int numeroProductos = Integer.parseInt(textoNumeroProductos);
		//Aqui creo el arrayList i hago un for para rellenar cada array dentro del array, tambien guardo el precio con iva 
		//para hacer el precio total
		ArrayList<String[]> producto = new ArrayList<String[]>();
		for (int i = 0; i < numeroProductos; i++) {
			producto.add(introducirProducto(articulos));
			totalAPagar = totalAPagar + Double.parseDouble(producto.get(i)[3]);
		}
		//Digo cuanto tiene que pagar i le muestro la lista con todos los productos i el total a pagar
		String textoPagado = JOptionPane.showInputDialog(null, "Tienes que pagar " + totalAPagar);
		double pagado = Integer.parseInt(textoPagado);
		mostrarLista(producto, numeroProductos);
		System.out.println("Has pagado " + pagado + " tu cambio es de " + (pagado - totalAPagar));
	}
	public static String[] introducirProducto(Hashtable<String,Double> articulos) {
		String[] listaProducto = new String[4];
		String producto;
		boolean estaLista = true;
		do {
			//He a�adido una comprovaci�n para mirar si el producto est� en la lista
			producto = JOptionPane.showInputDialog(null, "Que producto has comprado");
			if (articulos.get(producto) == null) {
				JOptionPane.showMessageDialog(null, "El producto no esta en stock");
			}else {
				estaLista = false;
			}
		} while (estaLista);
		listaProducto[0] = producto;
		String iva = JOptionPane.showInputDialog(null, "Que iva tiene el producto?(21 o 4)");
		listaProducto[1] = iva;
		String precioProducto = String.valueOf(articulos.get(producto));
		listaProducto[2] = precioProducto;
		double precioTotal = ((Double.parseDouble(precioProducto) / 100) * Double.parseDouble(iva)) + Double.parseDouble(precioProducto);
		listaProducto[3] = String.valueOf(precioTotal);
		
		return listaProducto;
	}
	public static void mostrarLista(ArrayList<String[]> lista, int num) {
		
		for (int i = 0; i < num; i++) {
			for (int j = 0; j < num; j++) {
				System.out.println("Producto: " + lista.get(i)[0]);
				System.out.println("IVA: " + lista.get(i)[1]);
				System.out.println("Precio: " + lista.get(i)[2]);
				System.out.println("Precio con IVA: " + lista.get(i)[3]);
			}
			System.out.println(" ");
		}	
	}
	public static void mostrarHashtable(Hashtable<String,Double> table) {
		Enumeration<Double> elements = table.elements();
		Enumeration<String> llaves = table.keys();
		
		while (llaves.hasMoreElements()) {
			System.out.println("El articulo: " + llaves.nextElement() + ", tiene una precio de: " + elements.nextElement());
			
		}
		System.out.println(" ");
		System.out.println(" ");
	}
}
