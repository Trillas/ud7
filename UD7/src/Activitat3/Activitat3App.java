package Activitat3;

import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JOptionPane;

public class Activitat3App {

	public static void main(String[] args) {
		//Creo las variables que necesitare
		boolean salir = true;
		String txtQueHace;
		//Creo el Hashtable y lo lleno de productos
		Hashtable<String,Double> articulos = new Hashtable<String,Double>(){
			{
			put("CocaCola", 2.00);
			put("Pepsi", 1.4);
			put("Pollo", 4.3);
			put("Pan", 0.5);
			put("Ajo", 1.3);
			put("Pi�a", 2.5);
			put("Tomate", 3.1);
			put("Yogurt", 1.7);
			put("Helado", 3.5);
			put("Pescado", 4.9);
			}
		};
		do {
			//Hago la pregunta del menu, i compruebo que sea valido
			do {
				txtQueHace = JOptionPane.showInputDialog(null, "Que quieres hacer?(1: a�adir, 2: comprobar un producto, 3: mirar todos los productos, 4: salir)");
			} while (!txtQueHace.contentEquals("1") && !txtQueHace.contentEquals("2") && !txtQueHace.contentEquals("3") && !txtQueHace.contentEquals("4"));
			int queHace = Integer.parseInt(txtQueHace);
			switch (queHace) {
			//Hago un switch con el men� y las diferentes opciones
			case 1:
				//Pregunto nombre y precio del producto y los a�ado
				String nombreProducto = JOptionPane.showInputDialog(null, "Que nombre tiene el producto?");
				String txtPrecioProducto = JOptionPane.showInputDialog(null, "Que precio tiene el producto?");
				double precioProducto= Double.parseDouble(txtPrecioProducto);
				articulos.put(nombreProducto, precioProducto);
				break;
			case 2:
				//Miro el producto
				String productoMirar = JOptionPane.showInputDialog(null, "Que producto quieres comprobar?");
				JOptionPane.showMessageDialog(null, "El articulo " + productoMirar + " tiene un precio de: " + articulos.get(productoMirar));
				break;
			case 3:
				//Muestro todos los productos
				mostrarHashtable(articulos);
				break;
			case 4:
				//Esto es que quiere salir, asi que le ense�o un mensage
				JOptionPane.showMessageDialog(null, "Adios");
				salir = false;
				break;
			}
			
		} while (salir);
		
	}
	//Para mostrar el hashtable
	public static void mostrarHashtable(Hashtable<String,Double> table) {
		Enumeration<Double> elements = table.elements();
		Enumeration<String> llaves = table.keys();
		
		while (llaves.hasMoreElements()) {
			System.out.println("El articulo: " + llaves.nextElement() + ", tiene una precio de: " + elements.nextElement());
			
		}
		System.out.println(" ");
		System.out.println(" ");
	}

}
